package ru.batrdmi.svnplugin;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BranchHighlighter extends MouseAdapter {
    private final SVNRevisionGraph graph;
    private int currentRow = -1;

    public BranchHighlighter(SVNRevisionGraph graph) {
        this.graph = graph;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        int oldRow = currentRow;
        currentRow = -1;
        if (currentRow != oldRow) {
            updateRow(oldRow);
            updateRow(currentRow);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        int oldRow = currentRow;
        currentRow = graph.getBranchForY(e.getY());
        if (currentRow != oldRow) {
            updateRow(oldRow);
            updateRow(currentRow);
        }
    }

    private void updateRow(int row) {
        if (row >= 0) {
            graph.repaintBranch(row);
        }
    }
    
    public int getHighlightedRow() {
        return currentRow;
    }
}
