package ru.batrdmi.svnplugin.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.ToggleAction;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.util.IconLoader;
import ru.batrdmi.svnplugin.SVNRevisionGraph;

public class DisplayOnlyImpactingRevisionsAction extends ToggleAction implements DumbAware {
    private final SVNRevisionGraph graph;

    public DisplayOnlyImpactingRevisionsAction(SVNRevisionGraph graph) {
        super("Display only impacting revisions", "Display only revisions that impact current",
                IconLoader.getIcon("/icons/impact.png"));
        this.graph = graph;
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        e.getPresentation().setEnabled(!graph.isRefreshInProgress());
    }

    @Override
    public boolean isSelected(AnActionEvent anActionEvent) {
        return graph.isDisplayingOnlyImpactingRevisions();
    }

    @Override
    public void setSelected(AnActionEvent anActionEvent, boolean b) {
        graph.setDisplayOnlyImpactingRevisions(b);
    }
}
