package ru.batrdmi.svnplugin.logic;

public enum ScanMode {
    ONLY_IMPACTING_PATHS,INCLUDE_CURRENT_BRANCHES,INCLUDE_CURRENT_BRANCHES_AND_TAGS
}
