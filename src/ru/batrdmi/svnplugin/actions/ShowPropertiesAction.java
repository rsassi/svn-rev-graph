package ru.batrdmi.svnplugin.actions;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.FrameWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.vcs.FilePath;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.idea.svn.SvnVcs;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperties;
import ru.batrdmi.svnplugin.SVNRevisionGraph;
import ru.batrdmi.svnplugin.gui.PropertiesComponent;
import ru.batrdmi.svnplugin.logic.FileHistoryRetriever;
import ru.batrdmi.svnplugin.logic.Revision;

import javax.imageio.ImageIO;
import java.io.IOException;

public class ShowPropertiesAction extends AnAction implements DumbAware {
    private static final Logger log = Logger.getInstance("ru.batrdmi.svnplugin.actions.ShowPropertiesAction");

    public ShowPropertiesAction() {
        super("Show properties", "Show versioned SVN properties for selected revision", IconLoader.getIcon("/icons/show_props.png"));
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        Presentation presentation = e.getPresentation();
        Revision revision = e.getData(SVNRevisionGraph.SELECTED_REVISION);
        FilePath filePath = e.getData(SVNRevisionGraph.SRC_FILE);
        boolean enabled = filePath!= null && revision != null && !revision.isDeleted();
        presentation.setEnabled(enabled);
        presentation.setVisible(enabled || !ActionPlaces.isPopupPlace(e.getPlace()));
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        final Project project = event.getData(PlatformDataKeys.PROJECT);
        SvnVcs svn = SvnVcs.getInstance(project);
        final Revision revision = event.getData(SVNRevisionGraph.SELECTED_REVISION);
        FilePath filePath = event.getData(SVNRevisionGraph.SRC_FILE);
        if (filePath == null || revision == null) {
            return;
        }
        final FileHistoryRetriever historyRetriever = new FileHistoryRetriever(svn, filePath.getVirtualFile());
        new Task.Modal(project, "Loading properties for " + revision, false) {
            SVNProperties result;

            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                try {
                    result = historyRetriever.getFileProperties(revision);
                } catch (SVNException e) {
                    log.error("Error retrieving properties", e);
                }
            }

            @Override
            public void onSuccess() {
                if (result == null) {
                    Messages.showErrorDialog(project, "Error retrieving properties from repository",
                            SVNRevisionGraphAction.ERROR_DIALOG_TITLE);
                } else {
                    showProperties(project, revision, result);
                }
            }
        }.queue();
    }

    private void showProperties(Project project, Revision revision, SVNProperties result) {
        FrameWrapper fw = new FrameWrapper(null, "SvnRevisionGraphPlugin.svnPropertiesDimensions");
        fw.setTitle(" SVN properties: " + revision);
        try {
            fw.setImage(ImageIO.read(getClass().getResource("/icons/show_props.png")));
        } catch (IOException e) {
            log.error("Error loading icon", e);
        }
        fw.setProject(project);
        PropertiesComponent propertiesComponent = new PropertiesComponent();
        fw.setComponent(propertiesComponent);
        fw.show();
        propertiesComponent.setData(result);
    }
}
