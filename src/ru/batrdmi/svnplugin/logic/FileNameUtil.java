package ru.batrdmi.svnplugin.logic;

public class FileNameUtil {
    public static String[] splitPath(String path) {
        String[] parts = new String[3];
        if (path.contains("/trunk/") || path.endsWith("/trunk")) {
            int i = path.indexOf("/trunk/");
            if (i >= 0) {
                parts[0] = path.substring(0, i);
                parts[1] = "/trunk";
                parts[2] = path.substring(i + 6);
            } else {
                parts[0] = path.substring(0, path.length() - 6);
                parts[1] = "/trunk";
                parts[2] = "";
            }
        } else if (path.contains("/branches/")) {
            int i = path.indexOf("/branches/");
            parts[0] = path.substring(0, i);
            int j = path.indexOf('/', i + 10);
            if (j >= 0) {
                parts[1] = path.substring(i, j);
                parts[2] = path.substring(j);
            } else {
                parts[1] = path.substring(i);
                parts[2] = "";
            }
        } else if (path.contains("/tags/")) {
            int i = path.indexOf("/tags/");
            parts[0] = path.substring(0, i);
            int j = path.indexOf('/', i + 6);
            if (j >= 0) {
                parts[1] = path.substring(i, j);
                parts[2] = path.substring(j);
            } else {
                parts[1] = path.substring(i);
                parts[2] = "";
            }
        } else {
            return null;
        }
        return parts;
    }
}
