package ru.batrdmi.svnplugin.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;
import ru.batrdmi.svnplugin.SVNRevisionGraph;

public class RefreshAction extends AnAction implements DumbAware {
    private final SVNRevisionGraph graph;

    public RefreshAction(SVNRevisionGraph graph) {
        super("Refresh", "Refresh file history", IconLoader.getIcon("/actions/sync.png"));
        this.graph = graph;
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        e.getPresentation().setEnabled(!graph.isRefreshInProgress());
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        graph.refresh();
    }
}
